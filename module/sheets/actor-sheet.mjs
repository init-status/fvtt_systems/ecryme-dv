import {onManageActiveEffect, prepareActiveEffectCategories} from "../system/effects.mjs";
import { EcrymeRoll } from "../system/roll.js";
import { EcrymeFight } from "../system/fight.mjs"
import { updateActorSkillScore, resetActorSkillUsure } from "../system/functions.mjs";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class EcrymeActorSheet extends ActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["engrenages", "sheet", "actor"],
      template: "systems/engrenages/templates/actor/actor-sheet.html",
      width: 600,
      height: 600,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "skills" }]
    });
  }

  /** @override */
  get template() {
    return `systems/engrenages/templates/actor/actor-${this.actor.type}-sheet.html`;
  }

  /* -------------------------------------------- */

  /** @override */
  getData() {
    // Retrieve the data structure from the base sheet. You can inspect or log
    // the context variable to see the structure, but some key properties for
    // sheets are the actor object, the data object, whether or not it's
    // editable, the items array, and the effects array.
    const context = super.getData();

    // Use a safe clone of the actor data for further operations.
    const actorData = this.actor.toObject(false);

    // Add the actor's data to context.data for easier access, as well as flags.
    context.system = actorData.system;
    context.flags = actorData.flags;
    context.impactTypes = CONFIG.COGS.impactTypes;
    context.skillTypes = CONFIG.COGS.skillTypes;

    // Prepare character data and items.
    if (actorData.type == 'character') {
      this._prepareItems(context);
      this._prepareCharacterData(context);
    } else {
      this._prepareNpcData(context);
    }

    context.granting_cephalie = game.settings.get('engrenages', 'granting_cephalie');
    
    // Prepare NPC data and items.
    if (actorData.type == 'npc') {
      this._prepareItems(context);
    }

    // Add roll data for TinyMCE editors.
    context.rollData = context.actor.getRollData();

    // Prepare active effects
    context.effects = prepareActiveEffectCategories(this.actor.effects);

    return context;
  }

  /**
   * Organize and classify Items for Character sheets.
   *
   * @param {Object} actorData The actor to prepare.
   *
   * @return {undefined}
   */
  _prepareCharacterData(actorData) {
    actorData.nations = CONFIG.COGS.nations;
    actorData.zero2ten = CONFIG.COGS.zero2ten;

    if (!!game.settings.get('engrenages', 'granting_cephalie')){
      actorData.cskills = CONFIG.COGS.cskills;
    }

    // calcule le nombre de points de compétences restant
    for(let i in actorData.system.skills){
      for(let j in actorData.system.skills[i].data){
        actorData.system.skills[i].data[j].burn = actorData.system.skills[i].data[j].value - actorData.system.skills[i].data[j].spent;
      }
    }

    for(let i in actorData.system.cskills.data){
      actorData.system.cskills.data[i].burn = actorData.system.cskills.data[i].value - actorData.system.cskills.data[i].spent;
    }
  }

   /**
   * Organize and classify Items for Character sheets.
   *
   * @param {Object} actorData The actor to prepare.
   *
   * @return {undefined}
   */
   _prepareNpcData(npcData) {
    npcData.nations = CONFIG.COGS.nations;
    npcData.zero2ten = CONFIG.COGS.zero2ten;
    npcData.achievementReroll = CONFIG.COGS.achievementReroll;
    npcData.conservationReroll = CONFIG.COGS.conservationReroll;
    
  }

  /**
   * Organize and classify Items for Character sheets.
   *
   * @param {Object} actorData The actor to prepare.
   *
   * @return {undefined}
   */
  _prepareItems(context) {
    // Initialize containers.
    const gear = [];
    const traits = [];
    const spleen = [];
    const purpose = [];
    const specializations = [];
    const contacts = [];
    const weapons = [];
    const impacts = [];
    const scars = [];
    const anences = [];
    const bohemes = [];

    // Iterate through items, allocating to containers
    for (let i of context.items) {
      i.img = i.img || DEFAULT_TOKEN;
      // Append to gear.
      if (i.type === 'item') {
        gear.push(i);
      }
      else if (i.type === 'trait') {
        switch(i.system.category){
          case 'purpose':
            purpose.push(i);
            break;
          case 'spleen':
            spleen.push(i);
              break;
          default:
            traits.push(i);
            break;
        }
        
      }
      else if (i.type === 'specialization') {
        specializations.push(i);
      }
      else if (i.type === 'contact') {
        contacts.push(i);
      }
      else if (i.type === 'weapon') {
        weapons.push(i);
      }
      else if (i.type === 'impact') {
        impacts.push(i);
      }
      else if (i.type === 'scar') {
        scars.push(i);
      }
      else if (i.type === 'anence') {
        anences.push(i);
      }
      else if (i.type === 'boheme') {
        bohemes.push(i);
      }
     /* // Append to cephalie.
      else if (i.type === 'spell') {
        if (i.system.spellLevel != undefined) {
          cephalie[i.system.spellLevel].push(i);
        }
      }*/
    }

    // Assign and return
    context.gear = gear;
    context.purpose = purpose;
    context.spleen = spleen;
    context.traits = traits;
    context.specializations = specializations;
    context.contacts = contacts;
    context.weapons = weapons;
    context.impacts = impacts;
    context.scars = scars;
    context.anences = anences;
    context.bohemes = bohemes;
  }

  /* -------------------------------------------- */

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Render the item sheet for viewing/editing prior to the editable check.
    html.find('.item-edit').click(ev => {
      const li = $(ev.currentTarget).parents(".item");     
      const item = this.actor.items.get(li.data("itemId"));
      item.sheet.render(true);
    });

    // -------------------------------------------------------------
    // Everything below here is only needed if the sheet is editable
    if (!this.isEditable) return;

    // Add Inventory Item
    html.find('.item-create').click(this._onItemCreate.bind(this));

    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.items.get(li.data("itemId"));
      item.delete();
      li.slideUp(200, () => this.render(false));
    });

    // Active Effect management
    html.find(".effect-control").click(ev => onManageActiveEffect(ev, this.actor));

    // Rollable abilities.
    html.find('.rollable').click(this._onRoll.bind(this));
    html.find('.confrontation').click(this._onFight.bind(this));

    // burn baby burn
    html.find('.usure').click(ev => {
      // trouver la bonne compétence
      let skillObject = this.actor.system.skills[$(ev.currentTarget).data('stype')].data[$(ev.currentTarget).data('skey')]; 

      // prendre la valeur spent et l'incrémenter
      if (skillObject.spent < skillObject.value){
        skillObject.spent++;
        // updateActorSkillScore(this.actor, $(ev.currentTarget).data('label'), 'spent', skillObject.spent);
      }
      
      this.actor.sheet.render(true); // recharger la vue pour mettre à jour la valeur
    });

    // burn engrenages burn
    html.find('.cusure').click(ev => {
      // trouver la bonne compétence
      let skillObject = this.actor.system.cskills.data[$(ev.currentTarget).data('label')]; 

      // prendre la valeur spent et l'incrémenter
      if (skillObject.spent < skillObject.value){
        skillObject.spent++;
        // updateActorSkillScore(this.actor, $(ev.currentTarget).data('label'), 'spent', skillObject.spent);
      }
      
      this.actor.sheet.render(true); // recharger la vue pour mettre à jour la valeur
    });

    // rest
    html.find('.rest').click(ev => {
      Dialog.confirm({
        title: game.i18n.localize("COGS.Dialog.RestTitle"),
        content: '<p>'+game.i18n.localize("COGS.Dialog.RestContent") + '</p>',
        yes: () => {
          resetActorSkillUsure(this.actor);
          this.actor.sheet.render(true); // recharger la vue pour mettre à jour les valeurs
        },
        no: () => { },
        defaultYes: false,
      });
      
    });
    
    

    // Drag events for macros.
    if (this.actor.isOwner) {
      let handler = ev => this._onDragStart(ev);
      html.find('li.item').each((i, li) => {
        if (li.classList.contains("inventory-header")) return;
        li.setAttribute("draggable", true);
        li.addEventListener("dragstart", handler, false);
      });
    }

    // npc choose skill
    html.find('.choose-skill').click(this.npcChooseSkill.bind(this.actor));

  }


  async npcChooseSkill(ev){
    const actorId = $(ev.currentTarget).data('actorId');
    const selectedActor = game.actors.get(actorId);
    // console.log(selectedActor, selectedActor.system);
    
    let template = `
          <form>
              <div class="form-group">
                  <label>${game.i18n.localize('COGS.Skill')}</label>
                  <select name="skillLabel" id="NPCSkillLabel">
                  ` + Handlebars.helpers.selectOptions(CONFIG.COGS.skillsList, {hash: {'localize': true}}) + `
                  </select>
              </div>
              <div class="form-group">
                  <label>${game.i18n.localize('COGS.SkillScore')}</label>
                  <input type="number" id="NPCSkillValue" value="{{ skill.value }}">
              </div>
          </form>`;
  
    let buttons = {};
    buttons = {
      create: {
        icon: '<i class="fas fa-check"></i>',
        label: `${game.i18n.localize('COGS.AddSkill')}`,
        callback: async (html) => {
          const NPCSkillLabel = html.find('#NPCSkillLabel')[0].value || 0;
          const NPCSkillValue = parseInt(html.find('#NPCSkillValue')[0].value || 0);

          // on recherche le label parmi les compétences
          for (let st in selectedActor.system.skills){
            for (let s in selectedActor.system.skills[st].data){
              if (selectedActor.system.skills[st].data[s].label == NPCSkillLabel){
                selectedActor.system.skills[st].data[s].value = NPCSkillValue; // printing the new value
                const systemSkillKey = `system.skills.${st}.data.${s}.value`;
                selectedActor.update({[systemSkillKey]:NPCSkillValue }); // updating actor's data
              }
            }
          }

          selectedActor.sheet.render(true); // recharger la vue pour mettre à jour la valeur
        },
      },
      cancel: {
        icon: '<i class="fas fa-times"></i>',
        label: `${game.i18n.localize('COGS.Cancel')}`,
      },
    };
  
    new Dialog({
      title: game.i18n.localize('COGS.ChooseSkill'),
      content: template,
      buttons: buttons,
      default: 'create',
    }).render(true);
  }

  /**
   * Handle creating a new Owned Item for the actor using initial data defined in the HTML dataset
   * @param {Event} event   The originating click event
   * @private
   */
  async _onItemCreate(event) {
    event.preventDefault();
    const header = event.currentTarget;
    // Get the type of item to create.
    const type = header.dataset.type;
    // Grab any data associated with this control.
    const data = duplicate(header.dataset);
    // Initialize a default name.
    const name = `New ${type.capitalize()}`;
    // Prepare the item object.
    const itemData = {
      name: name,
      type: type,
      system: data
    };
    // Remove the type from the dataset since it's in the itemData.type prop.
    delete itemData.system["type"];

    // Finally, create the item!
    return await Item.create(itemData, {parent: this.actor});
  }

  /**
   * Handle clickable rolls.
   * @param {Event} event   The originating click event
   * @private
   */
  _onRoll(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;

    let externalData = {};

    if (this.actor.type == 'character' || this.actor.type == 'npc'){
      if (dataset.skill != undefined){
        externalData.skill = dataset.skill;
      }

      if (dataset.skillType != undefined){ // c'est un type
        externalData.value = parseInt(dataset.value,10);  
        externalData.skill = game.i18n.format(dataset.skillType);  
      } else if (dataset.value != undefined){
        externalData.skillScore = dataset.value;
      }
    }

    if (dataset.specialization != undefined){
      externalData.specialization = dataset.specialization;
    }
    // on fait le passage de l'id de l'actor pour le récupérer plus tard
    externalData.speakerId = this.actor.id;
    EcrymeRoll.ui(externalData);

    /*// Handle item rolls.
    if (dataset.rollType) {
      if (dataset.rollType == 'item') {
        const itemId = element.closest('.item').dataset.itemId;
        const item = this.actor.items.get(itemId);
        if (item) return item.roll();
      }
    }

    // Handle rolls that supply the formula directly.
    if (dataset.roll) {
      let label = dataset.label ? `[ability] ${dataset.label}` : '';
      let roll = new Roll(dataset.roll, this.actor.getRollData());
      roll.toMessage({
        speaker: ChatMessage.getSpeaker({ actor: this.actor }),
        flavor: label,
        rollMode: game.settings.get('core', 'rollMode'),
      });
      return roll;
    }*/
  }

  /**
   * Handle clickable rolls.
   * @param {Event} event   The originating click event
   * @private
   */
  _onFight(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;

    let externalData = {};
    if (dataset.label != undefined){
      externalData.skill = dataset.label;
    }

    if (dataset.value != undefined){
      externalData.skill = dataset.value;
      }
    if (dataset.specialization != undefined){
      externalData.specialization = dataset.specialization;
    }

    // on fait le passage de l'id de l'actor pour le récupérer plus tard
    externalData.speakerId = this.actor.id;

    EcrymeFight.ui(externalData);
   
  }

}
