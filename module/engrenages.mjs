// Import document classes.
import { registerHandlebarsHelpers } from "./system/helpers.mjs";
import { registerHooks } from "./system/hooks.mjs"
import { registerSettings } from "./system/settings.mjs"
import { EcrymeActor } from "./documents/actor.mjs";
import { EcrymeItem } from "./documents/item.mjs";
import { EcrymeCombat } from "./system/fight.mjs";

// Import sheet classes.
import { EcrymeActorSheet } from "./sheets/actor-sheet.mjs";
import { EcrymeItemSheet } from "./sheets/item-sheet.mjs";
import { EcrymeTraitSheet } from "./sheets/trait-sheet.mjs";

// Import helper/utility classes and constants.
import { preloadHandlebarsTemplates } from "./system/templates.mjs";
import { COGS } from "./system/config.mjs";
// import { activateStandardScene } from './module/activateStandardScene.js';
import { EcrymeRoll } from "./system/roll.js"

/* -------------------------------------------- */
/*  Init Hook                                   */
/* -------------------------------------------- */

Hooks.once('init', async function() {

  // Add utility classes to the global game object so that they're more easily
  // accessible in global contexts.
  game.ecryme = {
    EcrymeActor,
    EcrymeItem,
    rollItemMacro
  };

  // Add custom constants for configuration.
  CONFIG.COGS = COGS;
  CONFIG.debug.hooks = false;

  /**
   * Set an initiative formula for the system
   * @type {String}
   */
  CONFIG.Combat.initiative = {
    formula: "2d6",
    decimals: 2
  };

  // Define custom Document classes
  CONFIG.Actor.documentClass = EcrymeActor;
  CONFIG.Item.documentClass = EcrymeItem;
  CONFIG.Combat.documentClass = EcrymeCombat;

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("engrenages", EcrymeActorSheet, { makeDefault: true });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("engrenages", EcrymeItemSheet, { makeDefault: true });
  Items.registerSheet("engrenages", EcrymeTraitSheet);  

  
  registerHandlebarsHelpers(); // Register Handlebars helpers
  registerHooks(); // register Hooks
  registerSettings(); // register Ecryme Settings

  // Preload Handlebars templates.
  return preloadHandlebarsTemplates();
});


/* -------------------------------------------- */
/*  Ready Hook                                  */
/* -------------------------------------------- */

Hooks.once("ready", async function() {

  // Wait to register hotbar drop hook on ready so that modules could register earlier if they want to
  Hooks.on("hotbarDrop", (bar, data, slot) => createItemMacro(data, slot));
});

/* -------------------------------------------- */
/*  Hotbar Macros                               */
/* -------------------------------------------- */

/**
 * Create a Macro from an Item drop.
 * Get an existing item macro if one exists, otherwise create a new one.
 * @param {Object} data     The dropped data
 * @param {number} slot     The hotbar slot to use
 * @returns {Promise}
 */
async function createItemMacro(data, slot) {
  // First, determine if this is a valid owned item.
  if (data.type !== "Item") return;
  if (!data.uuid.includes('Actor.') && !data.uuid.includes('Token.')) {
    return ui.notifications.warn("You can only create macro buttons for owned Items");
  }
  // If it is, retrieve it based on the uuid.
  const item = await Item.fromDropData(data);

  // Create the macro command using the uuid.
  const command = `game.ecryme.rollItemMacro("${data.uuid}");`;
  let macro = game.macros.find(m => (m.name === item.name) && (m.command === command));
  if (!macro) {
    macro = await Macro.create({
      name: item.name,
      type: "script",
      img: item.img,
      command: command,
      flags: { "engrenages.itemMacro": true }
    });
  }
  game.user.assignHotbarMacro(macro, slot);
  return false;
}

/**
 * Create a Macro from an Item drop.
 * Get an existing item macro if one exists, otherwise create a new one.
 * @param {string} itemUuid
 */
function rollItemMacro(itemUuid) {
  // Reconstruct the drop data so that we can load the item.
  const dropData = {
    type: 'Item',
    uuid: itemUuid
  };
  // Load the item from the uuid.
  Item.fromDropData(dropData).then(item => {
    // Determine if the item loaded and if it's an owned item.
    if (!item || !item.parent) {
      const itemName = item?.name ?? itemUuid;
      return ui.notifications.warn(`Could not find item ${itemName}. You may need to delete and recreate this macro.`);
    }

    // Trigger the item roll
    item.roll();
  });
}