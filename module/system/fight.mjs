import { COGS } from "./config.mjs";
import { getActorSkillScore,updateActorSkillScore,getActorSkillTypeScore,getSkillTypeFromLabel, getImpactTypeFromValue } from "./functions.mjs";
// import { CombatResultDialog } from "./dialogs.mjs";

export class EcrymeFight {

  static instance = null;

  static get() {
    if (!EcrymeFight.instance)
      EcrymeFight.instance = new EcrymeFight();
    return EcrymeFight.instance;
  }

 
  // data injected to char data
  static previousValues = {
    dicePool: 4,
    skills: COGS.skillsList,
    cskills: COGS.cskills,
    cephalic: false,
    achievementReroll: COGS.achievementReroll,
    conservationReroll: COGS.conservationReroll
  };

  static rollerTemplate = 'systems/engrenages/templates/fight.html';
  static combatResultTemplate = 'systems/engrenages/templates/fight-result.html';

  /**
   * calcule les modificateurs du camp allié
   */
  static getAlliesMod(){
    if (game.combat.alliesScores == undefined){
      throw Error("le lanceur de confrontation n'a pas été lancé");
    }
    // on part de la valeur de la compétence
    let alliesTotal = game.combat.alliesScores.basicSkillValue || 0;
    
    // on ajoute le nombre d'alliés 
    alliesTotal += game.combat.allies.length;

    // on ajoute les bonus de traits
    if (game.combat.alliesScores.traitsValue != null && parseInt(game.combat.alliesScores.traitsValue,10) != 0){
      alliesTotal += game.combat.alliesScores.traitsValue;
    }

    // s'il y a spécialisation, on rajoute +2
    if (game.combat.alliesScores.specialization == true){
      alliesTotal += 2;
    }
    // si le personnage a utilisé de l'usure, on ajoute les points dépensés
    if (game.combat.alliesScores.spentSkill != null && game.combat.alliesScores.spentSkill > 0){
      alliesTotal += game.combat.alliesScores.spentSkill;
    }
    // on ajoute l'incidence des armes
    if (game.combat.alliesScores.weapons != null && parseInt(game.combat.alliesScores.weapons,10) != 0){
      alliesTotal += game.combat.alliesScores.weapons;
    }

    // on retire les malus de blessure
    if (game.combat.alliesScores.wounded != null && parseInt(game.combat.alliesScores.wounded,10) != 0){
      alliesTotal -= game.combat.alliesScores.wounded;
    }

    return alliesTotal;
  }

  /**
   * calcule les modificateurs du camp ennemi
   */
  static getAdversariesMod(){
    if (game.combat.adversariesScores == undefined){
      throw Error("le lanceur de confrontation n'a pas été lancé");
    }
    // on part de la valeur de la compétence
    let adversariesTotal = game.combat.adversariesScores.basicSkillValue || 0;
    // on ajoute le nombre d'alliés 
    adversariesTotal += game.combat.adversaries.length;

    // on ajoute l'incidence des armes
    if (game.combat.adversariesScores.weapons != null && parseInt(game.combat.adversariesScores.weapons,10) != 0){
      adversariesTotal += game.combat.adversariesScores.weapons;
    }

    // on retire les malus de blessure
    if (game.combat.adversariesScores.wounded != null && parseInt(game.combat.adversariesScores.wounded,10) != 0){
      adversariesTotal -= game.combat.adversariesScores.wounded;
    }

    return adversariesTotal;
  }


  static resetGameInfo(){
    delete game.combat.allies;
    delete game.combat.alliesScores;
    delete game.combat.adversaries;
    delete game.combat.adversariesScores;
    delete game.combat.achievementDice;
    delete game.combat.achievementDiceCount;
    delete game.combat.achievementResult;
    delete game.combat.conservationDice;
    delete game.combat.conservationDiceCount;
    delete game.combat.conservationResult;
    delete game.combat.adversariesMargin;
    delete game.combat.enemyAchievement;
    delete game.combat.enemyConservation;
    delete game.combat.alliesMargin;
    delete game.combat.fighter;
  }

  async performTest(skill, params, actor) {
    const dicePool = (game.combat.alliesScores.spleen != undefined || game.combat.alliesScores.purpose != undefined) ? '5' : '4';
    const r = new Roll(dicePool +`d6`);
    let diceString = '';
    let dicePoolHint = '';
    let discardedRoll = false;    
    let bonus = 0;
    let bonusText = '/+';


    r.roll(); // dice are rolled

    bonusText += bonus;

    let targetText = game.i18n.format('COGS.Selected') + ' : ' + game.i18n.format(game.combat.alliesScores.skillKey) + " " + game.combat.alliesScores.basicSkillValue + bonusText;
    if (game.combat.alliesScores.specialization != undefined){
      targetText += " (S)"; 
    }
    // tri par ordre croissant
    r.terms[0].results.sort((a,b) => a.result - b.result );
        
    if (game.combat.alliesScores.purpose != undefined){
      discardedRoll = r.terms[0].results.shift();
      dicePoolHint = ' - ' + game.i18n.format('COGS.PurposeTrait');
    } else if (game.combat.alliesScores.spleen != undefined){
      discardedRoll = r.terms[0].results.pop();
      dicePoolHint = ' - ' + game.i18n.format('COGS.SpleenTrait');
    } 
    const discardedRollText = (discardedRoll.result != undefined) ? '<div class="discarded-roll">' + discardedRoll.result + '</div>' : "";

    // génération de l'html pour les dés  
    for (let i = 0; i < r.terms[0].results.length; i++) {
      let result = r.terms[0].results[i].result;
      diceString += '<li class="roll die d6 die-'+ i +' ' + ((i < 2) ? "min": "max") + '">' + result + '</li>'; 
    }

    let hintText = game.i18n.format('COGS.ConfrontationHint');

    // save dice data for later
    game.combat.alliesScores.dice = r.terms[0].results;    
    
    // Build a dynamic html using the variables from above.
    const html = `
            <div class="engrenages roll confrontation">
                <div class="dice-roll">
                    <div class="dice-result">
                        <div class="dice-formula">
                        ` + dicePool + `d6 ` + dicePoolHint + `
                        </div>
                        <div class="dice-tooltip expanded">
                            <section class="tooltip-part">
                                <div class="parameters">
                                  ` + targetText + `
                                </div>
                                <div class="dice flexrow flex-between items-center">
                                    <ol class="dice-rolls">` + diceString + `</ol>
                                    <div class="discards text-right">` + discardedRollText + `</div>
                                </div>
                            </section>
                        </div>` +
                        `<p class="step1-text" id="step1">` + hintText + `</p>
                          <div class="row">
                              <a class="inline-block button add-to-achievement">` + game.i18n.format('COGS.Achievement') + `</a>
                              <a class="inline-block button add-to-conservation">` + game.i18n.format('COGS.Conservation') + `</a>
                              <a class="inline-block button reset"><i class="fa-solid fa-rotate-right"></i></a>
                              <a class="inline-block button resolve"><i class="fa-solid fa-check"></i></a>
                          </div>                          
                    </div>
                </div>
            </div>
        `;

    // Check if the dice3d module exists (Dice So Nice). If it does, post a roll in that and then
    // send to chat after the roll has finished. If not just send to chat.
    if (game.dice3d) {
      game.dice3d.showForRoll(r).then((displayed) => {
        this.sendToChat(html, r, actor);
      });
    } else {
      this.sendToChat(html, r, actor);
    };

    // on fait les comptes

  }

  async sendToChat(content, roll, actor) {
    let conf = {
      user: game.user._id,
      content: content,
      roll: roll,
      // sound: 'sounds/dice.wav'
    };

    if (actor)
      conf.speaker = ChatMessage.getSpeaker({ actor: actor });
    // Send's Chat Message to foundry, if items are missing they will appear as false or undefined and this not be rendered.
    ChatMessage.create(conf).then((msg) => {
      msg.setFlag('engrenages', 'round', game.combat.round);
      return msg;
    });        
  }
  
  static async chatMessageHandler(message, html, data) {
    // console.log("accès au fin du fin", message._id);

    // sélection du dé actif
    html.on("click", '.confrontation .die.d6', event => {
      const diceResult = parseInt($(event.target).html(),10);
      html.find('.confrontation .die.d6').removeClass('active');
      $(event.target).addClass('active');
    });

    // sélection des dés d'accomplissement
    html.on("click", '.confrontation .add-to-achievement', event => {
      const diceResult = parseInt(html.find('.confrontation .die.d6.active').html(),10);
      html.find('.confrontation .die.d6.active').removeClass('min').addClass('max');
    });

    // sélection des dés de conservation
    html.on("click", '.confrontation .add-to-conservation', event => {
      const diceResult = parseInt(html.find('.confrontation .die.d6.active').html(),10);
      html.find('.confrontation .die.d6.active').removeClass('max').addClass('min');
    });

    // reset de la sélection des pools
    html.on("click", '.confrontation .reset', event => {
      html.find('.confrontation .die.d6')
      .removeClass('max')
      .removeClass('min');
    });

    // résolution de la confrontation
    html.on("click", '.confrontation .resolve', async event => {
      game.combat['achievementDiceCount'] = html.find('.confrontation .die.d6.max').length;
      game.combat['conservationDiceCount'] = html.find('.confrontation .die.d6.min').length;

      if (game.combat['achievementDiceCount'] != 2 ||game.combat['conservationDiceCount'] != 2){
        console.log("la sélection des dés n'est pas bonne");
        return false;
      } else {
        game.combat.achievementDice = 0;
        game.combat.conservationDice = 0;
      }
      html.find('.confrontation .die.d6.max').each(function(index){
        game.combat.achievementDice += parseInt($(this).html(), 10);
      });
      html.find('.confrontation .die.d6.min').each(function(index){
        game.combat.conservationDice += parseInt($(this).html(), 10);
      });

      // saisie des résultats  
      if (game.combat.alliesScores == undefined){
        throw Error("le lanceur de confrontation n'a pas été lancé");
      }
      game.combat['achievementResult'] = EcrymeFight.getAlliesMod() + game.combat.achievementDice;
      game.combat['conservationResult'] = EcrymeFight.getAlliesMod() + game.combat.conservationDice;
      game.combat['enemyAchievement'] = EcrymeFight.getAdversariesMod() + game.combat.adversariesScores.achievementValue;
      game.combat['enemyConservation'] = EcrymeFight.getAdversariesMod() + game.combat.adversariesScores.conservationValue;
      game.combat['alliesMargin'] = game.combat.achievementResult - game.combat.enemyConservation;
      game.combat['adversariesMargin'] = game.combat.enemyAchievement - game.combat.conservationResult;

      // règle de la compétence qui limite la marge
      if (game.combat.alliesMargin > game.combat.alliesScores.basicSkillValue){
        game.combat.alliesMargin = game.combat.alliesScores.basicSkillValue;
      }
      if (game.combat.adversariesMargin > game.combat.adversariesScores.basicSkillValue){
        game.combat.adversariesMargin = game.combat.adversariesScores.basicSkillValue;
      }      

      let finishHtml = await renderTemplate(EcrymeFight.combatResultTemplate, { "combat": game.combat });

      let roundFinish = new Dialog({
        title: "Résolution du round",
        content: finishHtml,
        buttons: {
          finish: {
            label: "Finir le round",
            callback: (html) => {
              // let form = html.find('#dice-pool-form');
              // console.log("gestion du chatmessage", message.id, message);
             // ChatMessage.delete(message.id);
              game.combat.nextRound();
            }
          }/*,
          cancel: {
            label: game.i18n.localize('Close'),
            callback: () => { }
          }*/
        },
        render: function (h) {
          const skillType = getSkillTypeFromLabel(game.combat.fighter, game.combat.alliesScores.skillKey) || 'physical';

          h.on("click", '.create-impact.ally', event => {
            const totalDamage = (game.combat.adversariesScores.weapon != undefined) ? game.combat.adversariesMargin + game.combat.adversariesScores.weapon : game.combat.alliesMargin;
            const wound = {
              name: "Impact",
              type: 'impact',
              system: {
                  description: "généré au round " + game.combat.round + " du combat contre " + game.combat.adversaries[0].name,
                  skillType: skillType,
                  impactType: getImpactTypeFromValue(totalDamage),
                  value: totalDamage
              }
            };
            if (totalDamage > 0){
              game.combat.fighter.createEmbeddedDocuments("Item", [wound]);
            }  
            // console.log("impact allié", totalDamage);
          });   
          h.on("click", '.create-impact.adversary', event => {
            // const adversary = game.actors.get(game.combat.adversaries[0].id);
            const adversary = canvas.scene.tokens.get(game.combat.adversaries[0].token);
            const totalDamage = (game.combat.alliesScores.weapon != undefined) ? game.combat.alliesMargin + game.combat.alliesScores.weapon : game.combat.alliesMargin;

            const wound = {
              name: "Impact",
              type: 'impact',
              system: {
                  description: "généré au round " + game.combat.round + " du combat contre " + game.combat.fighter?.name,
                  skillType: skillType,
                  impactType: getImpactTypeFromValue(totalDamage),
                  value: totalDamage
              }
            };
            if (totalDamage > 0){
              adversary.actor.createEmbeddedDocuments("Item", [wound]);
            }
            // console.log("impact adversaire", totalDamage);
          });     
        }
      }, { width: 601, height: 'fit-content' });
      roundFinish.render(true);

    });


    // fin de la résolution de la confrontation


  }

  static async chatListeners(html) {
    // supprime le masquage des résultats du dé
    html.off("click", ".dice-roll");
  }
  
  /**
   * main class function
   * @returns 
   */
  static async ui(externalData = {}) {
    let actor = {};

    // get the actor
    try {
      actor = game.user.character;
    } catch(e){
      throw("Aucun personnage défini !");
    }

    if (actor == null && externalData.speakerId != undefined && externalData.speakerId != null){
      // on récupère le speakerId, et de là l'objet actor
      actor = game.actors.get(externalData.speakerId);     
      EcrymeFight.previousValues['speakerName'] = actor.name;
      EcrymeFight.previousValues['speakerImg'] = actor.img;      
    } else {
      EcrymeFight.previousValues['speakerName'] = "Anonyme";
    }

    // get the data
    let charData = (externalData) => {
      let o = Object.assign({ _template: EcrymeFight.rollerTemplate }, {...EcrymeFight.previousValues, ...externalData});
      return o;
    };
    let data = charData(externalData);    
    console.log("before ui", data);  

    // render template    
    let html = await renderTemplate(data._template, data);
    
    let ui = new Dialog({
      title: game.i18n.localize("COGS.FightTool"),
      content: html,
      buttons: {
        roll: {
          label: game.i18n.localize('COGS.Roll4Fight'),
          callback: (html) => {
            let form = html.find('#dice-pool-form');
            if (!form[0].checkValidity()) {
              throw "Invalid Data";
            }
            
            // on définit les deux grands tableaux de données
            game.combat.alliesScores = {};
            game.combat.adversariesScores = {};
            game.combat.alliesScores.weapon = 0;

            form.serializeArray().forEach(e => {
              switch (e.name) {
                case "skill":
                case "cephalic":
                  if (e.value !== ''){
                    game.combat.alliesScores.skillKey = e.value;
                  }
                  break;
                case "skill-score":
                  game.combat.alliesScores.basicSkillValue = +e.value;
                  break;
                case "specialization":
                  game.combat.alliesScores.specialization = true;
                  break;
                case "usure":
                  game.combat.alliesScores.spentSkill = +e.value;
                  break;
                case "trait":
                    game.combat.alliesScores.traitsValue = +e.value;
                    break;
                case "weapon":                  
                  game.combat.alliesScores.weapon = +e.value;
                  break;                  
                case "purpose":
                    game.combat.alliesScores.purpose = true;
                  break;
                case "spleen":
                  game.combat.alliesScores.spleen = true;
                  break;    
                case "adv-skill":
                  game.combat.adversariesScores.basicSkillValue = +e.value;
                    break;
                case "achievement":
                    game.combat.adversariesScores.achievementValue = +e.value;
                    break;
                case "conservation":
                    game.combat.adversariesScores.conservationValue = parseInt(e.value,10);
                    break;
                case "adv-weapon":
                      game.combat.adversariesScores.weapon = +e.value;
                      break;
                  case "adv-wounded":
                      game.combat.adversariesScores.wounded = parseInt(e.value,10);
                      break;    
                }
            });
            // prise en compte de l'usure sur la feuille de perso
             if (game.combat.alliesScores.spentSkill != undefined){
               const newSpentScore =  getActorSkillScore(actor, game.combat.alliesScores.skillKey, 'spent') + game.combat.alliesScores.spentSkill;
               updateActorSkillScore(actor, game.combat.alliesScores.skillKey, 'spent', newSpentScore);
            }

            if (game.combat.alliesScores.skillKey == undefined){
              window.confirm("pensez à sélectionner une compétence");
              return false;
            } else {
              return EcrymeFight.get().performTest(actor);
            }
            
          }
        }
      },
      render: function (h) {
        h.on("change", 'select[name="skill"]', event => {
          const skillLabel = $(event.target).val();

          // on modifie le score chez l'allié
          const currentSkillScore = getActorSkillScore(actor, skillLabel) - getActorSkillScore(actor, skillLabel, 'spent');
          if (parseInt(currentSkillScore,10) >= 0){
            h.find('input#skillScore').val(currentSkillScore);
          }

          // on modifie le score chez l'adversaire
          const adversary = game.actors.get(data.adversary.id);
          let advSkillScore = getActorSkillScore(adversary,skillLabel);
          if (parseInt(advSkillScore,10) > 0){
            h.find('input#advSkillScore').val(advSkillScore);
          } else {
            // on recherche le type de la compétence
            const advSkillType = getSkillTypeFromLabel(adversary, skillLabel);
            advSkillScore = getActorSkillTypeScore(adversary,advSkillType);
            h.find('input#advSkillScore').val(advSkillScore);
          }         
        });        
      }
    }, { width: 601, height: 'fit-content' });
    ui.render(true);
    return ui;
  }
}

export class EcrymeCombat extends Combat {
  _encounterCheck(){
    console.log('encounter combat object', this);
  }

  async rollInitiative(ids, formula = undefined, messageOptions = {}) {
    // console.log(`${game.system.title} | Combat.rollInitiative()`, ids, formula, messageOptions);
    // Structure input data
    ids = typeof ids === "string" ? [ids] : ids;

    // étape 1 : on vérifie que le combattant est un pj
    /*if (ids.length == 1){
      console.log("il n'y a qu'un actor en lice");
    } else {
      console.log("il faut prendre le premier pj pour lancer la confrontation");
    }*/
    const combatant = this.combatants.get(ids[0]);
    let token = canvas.scene.tokens.get(combatant.tokenId);
    combatant.type = game.actors.get( combatant.actorId)?.type;
    combatant.disposition = token.disposition;
    let enemies = [];
    let adversary = null;
    let henchmen = [];
    // console.log("basis", canvas, game, this);

    this.combatants.filter((cbt) => {
      
      let token = canvas.scene.tokens.get(cbt.tokenId);
      let enemy = token.actor;
      const isEnemy = (token.disposition == -1) ? true : false;
      if (isEnemy){
        enemies.push({
          id: enemy.id,
          name: enemy.name,
          img: enemy.img,
          token: token.id
        })
        if (adversary == null){
          adversary = {
            id: enemy.id,
            name: enemy.name,
            img: enemy.img,            
            token: token.id,
            achievement: parseInt(enemy.system.reroll.achievement.value) + 6,
            conservation: 8 - parseInt(enemy.system.reroll.conservation.value),
            weapon: 0,
            wounded: 0
          } 
        } else {
          henchmen.push({
            id: enemy.id,
            name: enemy.name,
            img: enemy.img,
            token: token.id
          })
        }
      }
      return isEnemy;
    });

    let allies = this.combatants.filter((cbt) => {
      let token = canvas.scene.tokens.get(cbt.tokenId);
      return (token.disposition == 1 && cbt.id != combatant.id) ? true : false;
    });

    if ( game.combat.round < 1){
      let warningDialogHTML = await renderTemplate('systems/engrenages/templates/dialogs/warning.html', { 
        warningText: "Vous ne pouvez pas lancer une confrontation si le combat n'est pas commencé, ou s'il est terminé."
      });
      Dialog.prompt({   
        title: "Avertissement",     
        content: warningDialogHTML,
        label: 'Okay !',
      });
    } else if (combatant.type != 'character'){
      let warningDialogHTML = await renderTemplate('systems/engrenages/templates/dialogs/warning.html', { 
        warningText: "Seuls les PJs peuvent initier des confrontations. Relancer l'opération au tour du PJ actif."
      });
      Dialog.prompt({   
        title: "Avertissement",     
        content: warningDialogHTML,
        label: 'Okay !',
        callback: () => {
          // console.log('Il a compris');
        },
      });
    } else {
      // étape 2 : on envoie les infos
      let fightingActor = game.actors.get(combatant.actorId);
      game.combat.fighter = fightingActor;
      game.combat.adversaries = enemies;
      game.combat.allies = allies;
      
      EcrymeFight.ui({ 
        speakerId: combatant.actorId, 
        speakerWeapons: fightingActor.items.filter(item => item.type == 'weapon'),
        speakerExperience:fightingActor.system.attributes.experience.value,
        speakerEffects: token.actor.effects,
        adversary: adversary,
        henchmen: henchmen,
        allies: allies
      });
    }

  }

  nextRound() {
    /*let combatants = this.combatants.contents
    for (let c of combatants) {
      let actor = game.actors.get( c.actorId )
      actor.clearRoundModifiers()
    }*/
    // calcul des impacts par acteur et disqualification. 
    if (game.combat.fighter != undefined){
      const physicalJauge = game.combat.fighter.system.health.max;
      const mentalJauge = game.combat.fighter.system.mind.max;
      const socialJauge = game.combat.fighter.system.power.max;

      const impacts = game.combat.fighter.items.filter((item) => item.type == 'impact');
      const physicalImpact = impacts.filter((impact) => impact.system.skillType == 'physical').reduce((acc, item) => acc + item.system.value, 0);
      const mentalImpact = impacts.filter((impact) => impact.system.skillType == 'mental').reduce((acc, item) => acc + item.system.value, 0);
      const socialImpact = impacts.filter((impact) => impact.system.skillType == 'social').reduce((acc, item) => acc + item.system.value, 0);
      
      game.combat.fighter.system.health.value = physicalJauge - physicalImpact;
      game.combat.fighter.system.mind.value = mentalJauge - mentalImpact;
      game.combat.fighter.system.power.value = socialJauge - socialImpact;
      console.log("new values", game.combat.fighter.system);
    }
    // nettoyer le chat
    const MessagesOfCurrentRound = game.messages.filter((msg) => msg.getFlag('engrenages', 'round') == game.combat.round );
    
    for (let i in MessagesOfCurrentRound){
      console.log(MessagesOfCurrentRound[i].id);
      // ChatMessage.deleteDocuments([MessagesOfCurrentRound[i].id]);
      if(MessagesOfCurrentRound[i].id != undefined){
        // game.messages.delete(MessagesOfCurrentRound[i].id);
        ChatMessage.deleteDocuments([MessagesOfCurrentRound[i].id]);
      }
      
    }
    // nettoyage des valeurs stockées dans le game
    EcrymeFight.resetGameInfo();

    super.nextRound();
  }

  /************************************************************************************/
  startCombat() {
    /*let combatants = this.combatants.contents
    for (let c of combatants) {
      let actor = game.actors.get( c.actorId )
      actor.storeVitaliteCombat()
    }*/

    return super.startCombat();
  }
  
  /************************************************************************************/
  _onDelete() {
    let combatants = this.combatants.contents
    for (let c of combatants) {
      if (c.id != undefined){
        Combat.deleteDocuments([c.id]);
      }
    }
    super._onDelete()
  }
}