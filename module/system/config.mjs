export const COGS = {};

/**
 * The set of Ability Scores used within the sytem.
 * @type {Object}
 */

COGS.skills = {
  "physical": {
    "label": "COGS.SkillType.Physical",
    "data": {
      "COGS.AtleticsSkill":"COGS.AtleticsSkill",
      "COGS.DrivingSkill":"COGS.DrivingSkill",
      "COGS.FencingSkill":"COGS.FencingSkill",
      "COGS.BrawlSkill":"COGS.BrawlSkill",
      "COGS.ShootSkill":"COGS.ShootSkill"
    }
  },
  "mental": {
    "label": "COGS.SkillType.Mental",
    "data": { 
      "COGS.AnthropoMechanicologySkill":"COGS.AnthropoMechanicologySkill",
      "COGS.EcrymologySkill":"COGS.EcrymologySkill",
      "COGS.TraumatologySkill":"COGS.TraumatologySkill",
      "COGS.TraversologySkill":"COGS.TraversologySkill",
      "COGS.UrbatechnologySkill":"COGS.UrbatechnologySkill",
    }
  },
  "social": {
    "label": "COGS.SkillType.Social",
    "data": {
      "COGS.QuibbleSkill":"COGS.QuibbleSkill",
      "COGS.CreativitySkill":"COGS.CreativitySkill",
      "COGS.LoquacitySkill":"COGS.LoquacitySkill",
      "COGS.SkulduggerySkill":"COGS.SkulduggerySkill",
      "COGS.PerformanceSkill":"COGS.PerformanceSkill"
    }
  }
}

COGS.cskills = {
    "label": "COGS.CSkills",
    "data": {
      "COGS.ElegieSkill":"COGS.ElegieSkill",
      "COGS.MekaneSkill":"COGS.MekaneSkill",
      "COGS.EntelechieSkill":"COGS.EntelechieSkill",
      "COGS.PsycheSkill":"COGS.PsycheSkill",
      "COGS.ScorieSkill":"COGS.ScorieSkill"
    }
}

COGS.skillTypes = [
  { "key":"physical",
    "label": "COGS.SkillType.Physical"
  },
  { "key":"mental",
    "label": "COGS.SkillType.Mental",
  },
  { "key":"social",
    "label": "COGS.SkillType.Social",
  }
];

COGS.skillsList = {
  "COGS.AtleticsSkill":"COGS.AtleticsSkill",
  "COGS.DrivingSkill":"COGS.DrivingSkill",
  "COGS.FencingSkill":"COGS.FencingSkill",
  "COGS.BrawlSkill":"COGS.BrawlSkill",
  "COGS.ShootSkill":"COGS.ShootSkill",
  "COGS.AnthropoMechanicologySkill":"COGS.AnthropoMechanicologySkill",
  "COGS.EcrymologySkill":"COGS.EcrymologySkill",
  "COGS.TraumatologySkill":"COGS.TraumatologySkill",
  "COGS.TraversologySkill":"COGS.TraversologySkill",
  "COGS.UrbatechnologySkill":"COGS.UrbatechnologySkill",
  "COGS.QuibbleSkill":"COGS.QuibbleSkill",
  "COGS.CreativitySkill":"COGS.CreativitySkill",
  "COGS.LoquacitySkill":"COGS.LoquacitySkill",
  "COGS.SkulduggerySkill":"COGS.SkulduggerySkill",
  "COGS.PerformanceSkill":"COGS.PerformanceSkill"
}
COGS.traitsvalues = {
  '-2':'-2',
  '-1':'-1',
  '-0':'0',
  '1':'+1',
  '2':'+2',
}

COGS.zero2six = {
  '0':0,
  '1':1,
  '2':2,
  '3':3,
  '4':4,
  '5':5,
  '6':6
}

COGS.zero2ten = {
  '0':0,
  '1':1,
  '2':2,
  '3':3,
  '4':4,
  '5':5,
  '6':6,
  '7':7,
  '8':8,
  '9':9,
  '10':10
}

COGS.achievementReroll = {
  '1':7,
  '2':8,
  '3':9,
  '4':10,
  '5':11,
  '6':12
}

COGS.conservationReroll = {
  '1':7,
  '2':6,
  '3':5,
  '4':4,
  '5':3,
  '6':2
}
COGS.impactTypes = [
  { key:'light',label: "léger"},
  { key:'mild',label: "moyen"},
  { key:'heavy',label: "grave"},
  { key:'severe',label: "mortel"}
]

COGS.traitscategories = {
  "custom":"COGS.CustomTrait",
  "spleen":"COGS.SpleenTrait",
  "purpose":"COGS.PurposeTrait"
}

COGS.nations = {
  "istanie1":{
    "label": "Istanie (îles du couchant)",
    "cities": ["tanger", "argan", "ar'kobah", "ishandra"]
  },
  "istanie2":{
    "label": "Istanie (monts dinariques)",
    "cities": ["montenegro", "ishandra"]
  },
  "istanie3":{
    "label": "Istanie (anatolie)",
    "cities": ["ismyr", "istanbul", "ankara"]
  },
  "pentapolie":{
    "label": "Pentapolie",
    "cities": ["serone", "éole", "relais de l'affrevie", "relais de bragee", "géode", "théorie", "démos", "négoce", "lucé"]
  },
  "venice":{
    "label": "Venice",
    "cities": ["venice"]
  },
  "rhodesiennes":{
    "label": "Provinces rhodesiennes",
    "cities": ["alsyde", "spicule", "urbs", "les syénites"]
  },
  "methalune":{
    "label": "Méthalune",
    "cities": ["méthalune", "ferraille"]
  },
  "gloriana":{
    "label": "Gloriana",
    "cities": ["enclosure", "londres", "camelot", "hivernee"]
  },
  "antipolie":{
    "label": "Antipolie",
    "cities": ["paris", "ithar","candbury","abaya", "relais d'elphiel", "entrelace", "prague", "vienne"]
  },
  "olmune":{
    "label": "Principautés d'Olmune",
    "cities": ["entrepont", "olmune","arssens","braysine"]
  },
  "lansk":{
    "label": "Lansk",
    "cities": ["saint-petersbourg", "hypogée","sancre","moscou", "kiev","kryo"]
  },
  "nordanie":{
    "label": "Nordanie",
    "cities": ["souspente", "gottenborg","solth", "nacre", "dorvik", "mystille"]
  },
  "terraincognita":{
    "label": "Terra Incognita",
    "cities": ["chantier de transécryme"]
  }

}