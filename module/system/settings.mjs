export const registerSettings = function () {
    console.log('settings ok');
    game.settings.register("engrenages", "game-level", {
        name: game.i18n.localize("COGS.WorldSettings.GameLevel.Name"),
        hint: game.i18n.localize("COGS.WorldSettings.GameLevel.Hint"),
        scope: "system",
        config: true,
        type: String,
        choices: {
        "e": "Ecryme",
        "c": "Céphale",
        "b": "Bohème",
        "a": "Amertume"
        },
        default: 'e',
        onChange: value => {
        console.log(value);
        }
    });

    game.settings.register("engrenages", "granting_cephalie", {
        name: game.i18n.localize("COGS.WorldSettings.GrantingCephales.Label"),
        hint: game.i18n.localize("COGS.WorldSettings.GrantingCephales.Description"),
        scope: "system",
        config: true,
        type: Boolean,
        default: !1
    })

}  