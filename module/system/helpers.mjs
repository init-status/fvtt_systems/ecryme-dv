export const registerHandlebarsHelpers = function () {
    Handlebars.registerHelper('concat', (...args) => args.slice(0, -1).join(''));
    Handlebars.registerHelper('lower', e => e.toLocaleLowerCase());
  
    Handlebars.registerHelper('toLowerCase', function(str) {
        return str.toLowerCase();
    });
  
    // Ifis not equal
    Handlebars.registerHelper('ifne', function (v1, v2, options) {
      if (v1 !== v2) return options.fn(this);
      else return options.inverse(this);
    });
  
    // if equal
    Handlebars.registerHelper('ife', function (v1, v2, options) {
      if (v1 === v2) return options.fn(this);
      else return options.inverse(this);
    });

    // if greater than
    Handlebars.registerHelper('ifgt', function (v1, v2, options) {
      if (v1 > v2) return options.fn(this);
      else return options.inverse(this);
    });

    // if lower than
    Handlebars.registerHelper('iflt', function (v1, v2, options) {
      if (v1 < v2) return options.fn(this);
      else return options.inverse(this);
    });
    
     // sum
     Handlebars.registerHelper('sum', function (v1, v2) {
        return (parseInt(v1,10) + parseInt(v2,10)).toString();      
    });
}