# Unofficial (definitive) edition Ecryme System for FoundryVTTT

![Foundry v10](https://img.shields.io/badge/foundry-v10-green)

This system is using v10 Foundry VTT software. 

For the story, rumors said Ecryme editor was searching to adapt its game for FoundryVTT. I've never touched to Foundry before, so i wanted to see if i could make a system for one of my favorite TTRPGs. After, i've heard that the Ecryme editor choose a famous Foundry developer to develop a Foundry module. But community asked to keep a track of my work. Here it is. It's a snapshot of what could be a great system module for Ecryme. Future of this chunk of code will be released in the Cogs system module, based on the Cogs SRD that birthed with Ecryme v2 edition (hope you follow, guys...)

Of Course, it's only unofficial. Use it at your own risks. ;)

## Usage

manifest link for install : [https://gitlab.com/init-status/fvtt_systems/engrenages/-/raw/main/system.json](https://gitlab.com/init-status/fvtt_systems/engrenages/-/raw/main/system.json)  
zip archive of this FVTT system : [https://gitlab.com/api/v4/projects/45380693/repository/archive.zip](https://gitlab.com/api/v4/projects/45380693/repository/archive.zip)  

## Screenshots

![Beginners guide](screenshots/fvtt-ecryme-journal.png)  
![Archetype](screenshots/fvtt-ecryme-archetype.png)  
![Cephals](screenshots/fvtt-ecryme-cephalie.png)  
![Contacts](screenshots/fvtt-ecryme-contact.png)  
![Roller](screenshots/fvtt-ecryme-roller.png)  


## Thanks and inspirations 

- French Discord Server talking about Foundry VTT
- Illustrations and icons created by Pretre (Discord)
- Carter for this City of Mist sharing and his advices
- Bastien Durel for his Dune code sharing  (GNU LICENSE)