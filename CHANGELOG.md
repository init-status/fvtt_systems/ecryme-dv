# CHANGELOG

## definitive version
- améliorations graphiques sur les dialox bog et les feuilles d'objet
- correction dans la fenêtre de dialogue de Confrontation
- suppression des marges dans le message du chat

## 0.2.2
- améliorations graphiques sur la feuille de personnage

## 0.2.1
- lancer la confrontation depuis une rencontre
- Les pnjs ne peuvent plus lancer de confrontation
- réorganisation des fichiers système

## 0.2.0
- usure sur les jets et les confrontations
- récupération des scores de l'actor pour les compétences
- amélioration des calculs et des présentations pour les message chat

## 0.1.9
- usure sur les compétences
- PNJ : ajout de compétences (+ refactoring types de compétence)
- Ajout de Konzern dans les origines

## 0.1.8
- confrontation system

## 0.1.7
- journal layout

## 0.1.6
- dossier de personnage illustrations

## 0.1.5
- "dossier de personnage textes"

## 0.1.4
- css improvements 
- rollable skills and specializations

## 0.1.3
- archetypes added

## 0.1.2
- beginners guide updated

## 0.1.1
- path fixes

## 0.1.0
- Publish module

## 0.0.9
- Translated Paused Text
- Cephal Tab
- Anence sheet
- Boheme sheet

## 0.0.8
- Dice Roller, for Gamemaster

## 0.0.7
- contact sheet 
- sheet css improvements

## 0.0.6
- beginners pack included
- Usable npc sheet
- Usable zeppelin sheet

## 0.0.5
- contact sheet with variables
- Fencing variable fix

## 0.0.4
- zip path fixed

## 0.0.3
- raw file for manifest

## 0.0.2
- manifest

## 0.0.1

- Character sheets
- item sheets
- integration of Pretre media